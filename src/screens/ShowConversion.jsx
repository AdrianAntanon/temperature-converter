import React, { useState } from 'react';
import Converter from './Converter';

import '../assets/styles/styles.css';
import celsius from '../assets/images/celsius.svg';
import fahrenheit from '../assets/images/fahrenheit.svg';

const ShowConversion = () => {
  const [converterTemperature, setConverterTemperature] = useState(true);


  return (
    <div className="converter">
      <h1>{converterTemperature ? 'Celsius to Fahrenheit' : 'Fahrenheit to Celsius'}</h1>
      <button type="button" onClick={() => { setConverterTemperature(!converterTemperature) }}>
        <img alt="Degrees" style={{ width: 50, height: 50 }} src={converterTemperature ? celsius : fahrenheit} />
      </button>
      <Converter isCelsius={converterTemperature} />
    </div>
  );
};

export default ShowConversion;