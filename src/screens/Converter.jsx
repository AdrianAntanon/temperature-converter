import React, { useState } from 'react';
import { useFormik } from 'formik';

import Result from '../components/Result';

const Converter = (props) => {
  const [converter, setConverter] = useState();
  const { isCelsius } = props;

  const formik = useFormik({
    initialValues: {
      temperature: '',

    },
    onSubmit: values => {
      if (isNaN(values.temperature)) {
        alert('Please enter a number');
        return;
      }

      if (values.temperature === '') {
        alert('Please enter a number');
        return;
      }

      setConverter(values.temperature)
    },
  });

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <label htmlFor="temperature">Temperature</label>
        <input
          id="temperature"
          name="temperature"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.temperature}
        />
        <button type="submit">Calculate</button>
      </form>
      {converter ? (
        <Result
          isCelsius={isCelsius}
          result={converter}
        />
      ) : null
      }

    </div>
  );
};

export default Converter;