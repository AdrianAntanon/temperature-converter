import React from 'react';

import { toCelsius, toFahrenheit } from '../helpers';

const Result = (props) => {
  const { isCelsius, result } = props;
  const prevTemp = result;
  const convertedTemperature = isCelsius ? toFahrenheit(result) : toCelsius(result);

  return (
    <div>
      {isCelsius ?
        (
          <p>{prevTemp} ºC = {convertedTemperature} ºF </p>
        ) : (
          <p>{prevTemp} ºF = {convertedTemperature} ºC </p>
        )
      }
    </div>
  );
};

export default Result;